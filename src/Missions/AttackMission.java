package Missions;
import AerialVehicles.IAerial;
import Capabilities.IAttackCapability;
import Entities.Coordinates;

public class AttackMission extends Mission{

    private String target;

    public AttackMission(Coordinates destination, String pilotName, IAerial aerialVehicle, String target) {
        super(destination, pilotName, aerialVehicle);
        this.target = target;
    }

    @Override
    public void executeMission() throws AerialVehicleNotCompatibleException {

        if(this.getAerialVehicle().hasCapability(this.getAerialVehicle().getCapability())) {
            String pilotName = this.getPilotName();
            String platformName = this.getAerialVehicle().getClass().getSimpleName();
            IAttackCapability attackCapability = (IAttackCapability) this.getAerialVehicle().getCapability();
            String weapon = attackCapability.getRocket().label;
            int count = attackCapability.getMissileCount();
            System.out.println(pilotName + ": " + platformName + " Attacking " + target + " with: " + weapon + "X" + count);
        }else{
            String msg = "Aerial Vehicle unable to perform mission";
            throw new AerialVehicleNotCompatibleException(msg);
        }
    }
}

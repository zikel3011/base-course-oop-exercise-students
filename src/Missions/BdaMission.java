package Missions;
import AerialVehicles.IAerial;
import Capabilities.BdaCapability;
import Capabilities.IAttackCapability;
import Capabilities.IBdaCapability;
import Entities.Coordinates;

public class BdaMission extends Mission{

    private String target;

    public BdaMission(Coordinates destination, String pilotName, IAerial aerialVehicle, String target) {
        super(destination, pilotName, aerialVehicle);
        this.target = target;
    }

    @Override
    public void executeMission() throws AerialVehicleNotCompatibleException {
        if(this.getAerialVehicle().hasCapability(this.getAerialVehicle().getCapability())) {
            String pilotName = this.getPilotName();
            String platformName = this.getAerialVehicle().getClass().getSimpleName();
            IBdaCapability bdaCapability = (IBdaCapability) this.getAerialVehicle().getCapability();
            String camera = bdaCapability.getCamera().label;
            System.out.println(pilotName + ": " + platformName + " taking pictures of " + target + " with: " + camera+" camera");
        }else{
            String msg = "Aerial Vehicle unable to perform mission";
            throw new AerialVehicleNotCompatibleException(msg);
        }
    }
}

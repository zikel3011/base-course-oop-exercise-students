package Missions;
import AerialVehicles.IAerial;
import Entities.Coordinates;

public abstract class Mission implements IMission{

    private Coordinates destination;
    private String pilotName;
    private IAerial aerialVehicle;

    public Mission(Coordinates destination, String pilotName, IAerial aerialVehicle) {
        this.destination = destination;
        this.pilotName = pilotName;
        this.aerialVehicle = aerialVehicle;
    }

    @Override
    public void begin() {
        this.aerialVehicle.flyTo(this.destination);
    }

    public Coordinates getDestination() {
        return destination;
    }

    public String getPilotName() {
        return pilotName;
    }

    public IAerial getAerialVehicle() {
        return aerialVehicle;
    }

    @Override
    public void cancel() {
        this.aerialVehicle.land();
    }

    @Override
    public void finish() {
        try {
            this.executeMission();
            this.aerialVehicle.land();
        } catch (AerialVehicleNotCompatibleException e) {
            System.out.println("Unable to perform mission on selected platform");
            e.printStackTrace();
        }

    }
}


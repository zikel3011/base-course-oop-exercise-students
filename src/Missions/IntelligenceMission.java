package Missions;
import AerialVehicles.IAerial;
import Capabilities.BdaCapability;
import Capabilities.IAttackCapability;
import Capabilities.IBdaCapability;
import Capabilities.IIntelligenceCapability;
import Entities.Coordinates;

public class IntelligenceMission extends Mission{

    private String region;

    public IntelligenceMission(Coordinates destination, String pilotName, IAerial aerialVehicle, String target) {
        super(destination, pilotName, aerialVehicle);
        this.region = target;
    }

    @Override
    public void executeMission() throws AerialVehicleNotCompatibleException {
        if(this.getAerialVehicle().hasCapability(this.getAerialVehicle().getCapability())) {
            String pilotName = this.getPilotName();
            String platformName = this.getAerialVehicle().getClass().getSimpleName();
            IIntelligenceCapability intelligenceCapability = (IIntelligenceCapability) this.getAerialVehicle().getCapability();
            String sensor = intelligenceCapability.getSensor().label;
            System.out.println(pilotName + ": " + platformName + " collecting data in " + region + " with sensor type: " + sensor);
        }else{
            String msg = "Aerial Vehicle unable to perform mission";
            throw new AerialVehicleNotCompatibleException(msg);
        }
    }
}


import Capabilities.AttackCapability;
import Capabilities.BdaCapability;
import Capabilities.Capability;
import Capabilities.IntelligenceCapability;
import Enums.Camera;
import Enums.Sensor;
import FighterJets.F15;
import FighterJets.F16;
import FighterJets.FighterJet;

import Enums.Rockets;
import Entities.Coordinates;
import Missions.AttackMission;
import Missions.BdaMission;
import Missions.IMission;
import Missions.IntelligenceMission;
import UAV.Hermes.Kochav;
import UAV.UAV;

public class Main {

    public static void main(String[] args) {
        // Implement scenarios from readme here

        System.out.println("Good luck! ;)");



        // send kochav to this mission!
        try {
            Coordinates destCord = new Coordinates(33.2033427805222, 44.5176910795946);
            String pilotName = "Dror Zalicha";
            String target = "Iraq";
            Coordinates homeBase = new Coordinates(31.827604665263365, 34.81739714569337);
            Capability capability2 = new IntelligenceCapability(Sensor.InfraRed);
            UAV kochav = new Kochav(homeBase, capability2);
            IMission mission = new IntelligenceMission(destCord, pilotName, kochav, target);
            mission.begin();
            kochav.hoverOverLocation(destCord);
            mission.finish();
            System.out.println("Mission Ended");
        }
        catch (Exception e){
            e.printStackTrace();
        }
        // send f15 to this mission!

        try {
            Coordinates destCord = new Coordinates(33.2033427805222, 44.5176910795946);
            String pilotName = "Ze'ev Raz";
            String target = "Tuwaitha Nuclear Research Center";
            Coordinates homeBase = new Coordinates(31.827604665263365, 34.81739714569337);
            Capability capability = new AttackCapability(4,Rockets.SPICE250);
            FighterJet f15 = new F15(homeBase, capability);
            IMission mission = new AttackMission(destCord, pilotName, f15, target);
            mission.begin();
            mission.finish();
            System.out.println("Mission Ended");
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }
}

package Enums;

public enum Sensor {
    InfraRed("InfraRed"),
    Elint("Elint");
    public final String label;

    Sensor(String label) {
        this.label = label;
    }

    }

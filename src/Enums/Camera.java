package Enums;

public enum Camera {
    Regular("Regular"),
    NightVision("NightVision"),
    Thermal("Thermal");
    public final String label;

    Camera(String label) {
        this.label = label;
    }

    }

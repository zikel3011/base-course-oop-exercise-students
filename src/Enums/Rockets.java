package Enums;

public enum Rockets {
    PYTHON("Python"), AMRAM("Amram"), SPICE250("Spice250");

    public final String label;

    Rockets(String label) {
        this.label = label;
    }

}

package UAV.Hermes;

import Capabilities.*;
import Entities.Coordinates;
import UAV.UAV;

public class Kochav extends UAV {


    public Kochav(Coordinates defaultBase, Capability capability) {
        super(defaultBase, capability);
    }

    @Override
    public boolean hasCapability(Capability capability) {
        return capability instanceof IAttackCapability || capability instanceof IIntelligenceCapability ||
                capability instanceof IBdaCapability;
    }
}

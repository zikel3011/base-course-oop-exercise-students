package UAV.Hermes;


import Capabilities.Capability;
import Capabilities.IAttackCapability;
import Capabilities.IBdaCapability;
import Capabilities.IIntelligenceCapability;
import Entities.Coordinates;
import UAV.UAV;

public class Zik extends UAV {


    public Zik(Coordinates defaultBase, Capability capability) {
        super(defaultBase, capability);
    }

    @Override
    public boolean hasCapability(Capability capability) {
        return capability instanceof IIntelligenceCapability ||
                capability instanceof IBdaCapability;
    }}

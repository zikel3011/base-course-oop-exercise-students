package UAV;
import AerialVehicles.AerialVehicle;
import Capabilities.Capability;
import Entities.Coordinates;
import Enums.FlightStatus;

public abstract class UAV extends AerialVehicle {
    protected Capability capability;
    public UAV(Coordinates defaultBase,Capability capability) {
        super(defaultBase,capability);
    }

    public void hoverOverLocation(Coordinates destination){
        this.setFlightStatus(FlightStatus.AIR);
        System.out.println(String.format("Hovering over: {%.5f,%.5f}",destination.getLongitude(),destination.getLatitude() ));


    }

}

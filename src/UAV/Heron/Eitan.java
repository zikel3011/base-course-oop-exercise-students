package UAV.Heron;

import Capabilities.Capability;
import Capabilities.IAttackCapability;
import Capabilities.IIntelligenceCapability;
import Entities.Coordinates;
import UAV.UAV;

public class Eitan extends UAV {
    public Eitan(Coordinates defaultBase, Capability capability) {
        super(defaultBase, capability);
    }

    @Override
    public boolean hasCapability(Capability capability) {
        return capability instanceof IAttackCapability || capability instanceof IIntelligenceCapability;
    }
}

package UAV.Heron;

import Capabilities.*;
import Entities.Coordinates;
import UAV.UAV;

public class Shoval extends UAV{


    public Shoval(Coordinates defaultBase, Capability capability) {
        super(defaultBase, capability);
    }

    @Override
    public boolean hasCapability(Capability capability) {
        return capability instanceof IAttackCapability || capability instanceof IIntelligenceCapability ||
                capability instanceof IBdaCapability;
    }
}


package FighterJets;

import Capabilities.Capability;
import Capabilities.IAttackCapability;
import Capabilities.IBdaCapability;
import Entities.Coordinates;

public class F16 extends FighterJet{
    public F16(Coordinates defaultBase, Capability capability) {
        super(defaultBase, capability);
    }

    @Override
    public boolean hasCapability(Capability capability) {
        return capability instanceof IAttackCapability || capability instanceof IBdaCapability;
    }

}

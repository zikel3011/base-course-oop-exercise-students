package FighterJets;

import Capabilities.Capability;
import Capabilities.IAttackCapability;
import Capabilities.IIntelligenceCapability;
import Entities.Coordinates;

public class F15 extends FighterJet{
    public F15(Coordinates defaultBase, Capability capability) {
        super(defaultBase, capability);
    }

    @Override
    public boolean hasCapability(Capability capability) {
        return capability instanceof IAttackCapability || capability instanceof IIntelligenceCapability;
    }

}

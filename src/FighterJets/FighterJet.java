package FighterJets;

import AerialVehicles.AerialVehicle;
import Capabilities.Capability;
import Entities.Coordinates;
import Enums.FlightStatus;

public abstract class FighterJet extends AerialVehicle{
    protected Capability capability;
    public FighterJet(Coordinates defaultBase, Capability capability) {
        super(defaultBase,capability);
    }
}

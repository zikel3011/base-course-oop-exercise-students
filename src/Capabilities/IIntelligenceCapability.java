package Capabilities;

import Enums.Sensor;

public interface IIntelligenceCapability {
    Sensor getSensor();

    void setSensor(Sensor sensor);
}

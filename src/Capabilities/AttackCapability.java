package Capabilities;

import Enums.Rockets;

public class AttackCapability extends Capability implements IAttackCapability {
    private int missileCount;
    private Rockets rocket;

    public AttackCapability(int missileCount, Rockets rocket) {
        this.missileCount = missileCount;
        this.rocket = rocket;
    }

    @Override
    public int getMissileCount() {
        return missileCount;
    }

    @Override
    public void setMissileCount(int missileCount) {
        this.missileCount = missileCount;
    }

    @Override
    public Rockets getRocket() {
        return rocket;
    }

    @Override
    public void setRocket(Rockets rocket) {
        this.rocket = rocket;

    }

}
package Capabilities;

import Enums.Sensor;

public class IntelligenceCapability extends Capability implements IIntelligenceCapability{
    private Sensor sensor;
    public IntelligenceCapability(Sensor sensor) {
        this.sensor = sensor;
    }

    @Override
    public Sensor getSensor() {
        return sensor;
    }

    @Override
    public void setSensor(Sensor sensor) {
        this.sensor = sensor;
    }
}

package Capabilities;


import Enums.Camera;

public interface IBdaCapability {

    Camera getCamera();

    void setCamera(Camera camera);
}

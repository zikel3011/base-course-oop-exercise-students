package Capabilities;

import Enums.Rockets;

public interface IAttackCapability {
    int getMissileCount();

    void setMissileCount(int missileCount);

    Rockets getRocket();

    void setRocket(Rockets rocket);
}

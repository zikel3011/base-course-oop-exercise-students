package Capabilities;

import Enums.Camera;

public class BdaCapability extends Capability implements IBdaCapability {
    private Camera camera;

    public BdaCapability(Camera camera) {
        this.camera = camera;
    }
    @Override
    public Camera getCamera() {
        return camera;
    }

    @Override
    public void setCamera(Camera camera) {
        this.camera = camera;
    }
}

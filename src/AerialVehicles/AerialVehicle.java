package AerialVehicles;


import Capabilities.Capability;
import Entities.Coordinates;
import Enums.FlightStatus;

public abstract class AerialVehicle implements IAerial {
    protected int flightTime;
    protected FlightStatus flightStatus;
    protected Coordinates coordinates;
    protected double maxFlightTime;
    protected Capability capability;

    public AerialVehicle(Coordinates defaultBase, Capability capability) {
        this.flightTime = 0;
        this.flightStatus = FlightStatus.READY;
        this.coordinates = defaultBase;
        this.capability = capability;
    }

    @Override
    public Capability getCapability() {
        return capability;
    }
    public int getFlightTime() {
        return flightTime;
    }

    public void setFlightTime(int flightTime) {
        this.flightTime = flightTime;
    }

    public FlightStatus getFlightStatus() {
        return flightStatus;
    }

    public void setFlightStatus(FlightStatus flightStatus) {
        this.flightStatus = flightStatus;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public double getMaxFlightTime() {
        return maxFlightTime;
    }


    @Override
    public void flyTo(Coordinates destination) {
        if (this.flightStatus.equals(FlightStatus.READY)){
            System.out.println(String.format("Flying to: {%.5f,%.5f}",destination.getLongitude(),destination.getLatitude() ));
            this.flightStatus = FlightStatus.AIR;
        }
        else if (this.flightStatus.equals(FlightStatus.NOT_READY)){
            System.out.println("Aerial vehicle isn't ready to fly");
        }
        else{
            System.out.println("Aerial vehicle already in air");
        }
    }

    @Override
    public void land(Coordinates destination) {
        System.out.println(String.format("Landing on: {%.5f,%.5f}",destination.getLongitude(),destination.getLatitude() ));
        check();
    }
    @Override
    public void land() {
        this.land(this.coordinates);
    }

    @Override
    public void check() {
        if (flightTime>=maxFlightTime){
            flightStatus = FlightStatus.NOT_READY;
            repair();
        }
        else{
            flightStatus=FlightStatus.READY;
        }
    }

    @Override
    public void repair() {
        flightTime = 0;
        flightStatus = FlightStatus.READY;
    }
}

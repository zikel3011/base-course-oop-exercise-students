package AerialVehicles;

import Capabilities.Capability;
import Entities.Coordinates;

public interface IAerial {
    void flyTo(Coordinates coordinates);
    void land(Coordinates coordinates);
    void check();
    void repair();
    boolean hasCapability(Capability capability);
    Capability getCapability();
    void land();


}
